// Bai Tap 1
function sapXep() {
    var numb1, numb2, num3;

    numb1 = document.getElementById("txt-so-thu1").value * 1;
    numb2 = document.getElementById("txt-so-thu2").value * 1;
    numb3 = document.getElementById("txt-so-thu3").value * 1;

    if (numb1 < numb2 && numb1 < numb3) {
        if (numb2 < numb3) {
            document.getElementById("display").innerHTML = `${numb1} , ${numb2} , ${numb3}`;
        }
        else {
            document.getElementById("display").innerHTML = `${numb1} , ${numb3} , ${numb2}`;
        }
    }
    else if (numb2 < numb1 && numb2 < numb3) {
        if (numb1 < numb3) {
            document.getElementById("display").innerHTML = `${numb2} , ${numb1} , ${numb3}`;
        }
        else {
            document.getElementById("display").innerHTML = `${numb2} , ${numb3} , ${numb1}`;
        }
    }
    else if (numb3 < numb1 && numb3 < numb2) {
        if (numb1 < numb2) {
            document.getElementById("display").innerHTML = `${numb3} , ${numb1} , ${numb2}`;
        }
        else {
            document.getElementById("display").innerHTML = `${numb3} , ${numb2} , ${numb1}`;
        }
    }
}

// Bai Tap 2
function guiLoiChao() {
    var bo, me, anhTrai, emGai;

    if (document.getElementById("sel1").value == 1) {
        document.getElementById("display-2").innerHTML = `Chào Bố!`;
    } else if (document.getElementById("sel1").value == 2) {
        document.getElementById("display-2").innerHTML = `Chào Mẹ!`;
    } else if (document.getElementById("sel1").value == 3) {
        document.getElementById("display-2").innerHTML = `Chao Anh trai!`;
    } else if (document.getElementById("sel1").value == 4) {
        document.getElementById("display-2").innerHTML = `Chao Em gái!`;
    }
    if (document.getElementById("sel1").value == 0) {
        document.getElementById("display-2").innerHTML = `Chưa chọn thành viên kìa má!`;
    }
}

// Bai Tap 3
function kiemTraSoChanLe() {
    var soThu1, soThu2, soThu3, soChan = 0, soLe = 0, count = 0;

    soThu1 = document.getElementById("txt-first-number").value * 1;
    soThu2 = document.getElementById("txt-second-number").value * 1;
    soThu3 = document.getElementById("txt-third-number").value * 1;

    if (soThu1 % 2 == 0) {
        soChan++;
    } else {
        soLe++;
    }

    if (soThu2 % 2 == 0) {
        soChan++;
    } else {
        soLe++;
    }

    if (soThu3 % 2 == 0) {
        soChan++;
    } else {
        soLe++;
    }
    // document.getElementById("display-3").innerHTML = "Co " + soChan + " so chan, " + soLe + " so le"
    document.getElementById("display-3").innerHTML = `Có ${soChan} số chẳn`
    document.getElementById("display-3-3").innerHTML = `Có ${soLe} lẻ`
}

// Bai Tap 4
function kiemTraTamGiac() {

    var a, b, c;

    a = document.getElementById("txt-canh-1").value * 1;
    b = document.getElementById("txt-canh-2").value * 1;
    c = document.getElementById("txt-canh-3").value * 1;

    if (((a + b) > c) && ((a + c) > b) && ((b + c) > a)) {
        if (a == b && a == c && b == c) {
            // document.getElementById("dis").innerHTML = "Tam giac dieu";
            document.getElementById("display-4").innerHTML = `La tam giac deu`;
        } else if (a == b || a == c || b == c) {
            document.getElementById("display-4").innerHTML = `La tam giac can`;
        } else if (((Math.pow(a, 2)) == (Math.pow(b, 2) + Math.pow(c, 2))) || ((Math.pow(b, 2)) == (Math.pow(a, 2) + Math.pow(c, 2))) || ((Math.pow(c, 2)) == (Math.pow(a, 2) + Math.pow(b, 2)))) {
            document.getElementById("display-4").innerHTML = `La tam giac vuong`;
        } else {
            document.getElementById("display-4").innerHTML = `La tam giac khac`;
        }
    } else {
        document.getElementById("display-4").innerHTML = `Nhập sai rồi kìa! Tổng 2 cạnh này phải lớn hơn cạnh còn lại, mời nhập lại!`;
    }
}

// Bai Tap 5
function tomorrowBtn() {
    // alert("test1");
    var date = document.getElementById("txt-date").value * 1;
    var month = document.getElementById('txt-month').value * 1;
    var year = document.getElementById('txt-year').value * 1;

    switch (month) {
        case 1: case 3: case 5: case 7: case 8: case 10:
            if (1 <= date && date <= 30) {
                document.getElementById("output").innerHTML = (date + 1) + " / " + month + " / " + year;
            } else if (date == 31) {
                document.getElementById("output").innerHTML = (date - 30) + " / " + (month + 1) + " / " + year;
            }
            break;
        case 2:
            //Check / Nhuan ==> Co 29 ngay (2012, 2016, 2020)
            if (year % 4 == 0 && year / 100 != 0 || year % 400 == 0) {
                if (1 <= date && date <= 28) {
                    document.getElementById("output").innerHTML = (date + 1) + " / " + month + " / " + year;
                } else if (date == 29) {
                    document.getElementById("output").innerHTML = "1 / " + (month + 1) + " / " + year;
                }
            }
            else {
                if (1 <= date && date <= 27) {
                    document.getElementById("output").innerHTML = (date + 1) + " / " + month + " / " + year;
                } else if (date == 28) {
                    document.getElementById("output").innerHTML = (date - 27) + " / " + (month + 1) + " / " + year;
                }
            }
            break;
        case 4: case 6: case 9: case 11:
            if (1 <= date && date <= 29) {
                document.getElementById("output").innerHTML = (date + 1) + " / " + month + " / " + year;
            } else if (date == 30) {
                document.getElementById("output").innerHTML = (date - 29) + " / " + (month + 1) + " / " + year;
            }
            break;
        case 12:
            if (1 <= date && date <= 30) {
                document.getElementById("output").innerHTML = (date + 1) + " / " + month + " / " + year;
            } else if (date == 31) {
                document.getElementById("output").innerHTML = (date - 30) + " / " + (month - 11) + " / " + (year + 1);
            }
            break;
        default:
            alert("Nhap sai roi");
    }
}
function yesterdayBtn() {
    // alert("test2");
    var date = document.getElementById("txt-date").value * 1;
    var month = document.getElementById('txt-month').value * 1;
    var year = document.getElementById('txt-year').value * 1;

    switch (month) {
        case 1:
            if (1 < date && date <= 31) {
                document.getElementById("output").innerHTML = (date - 1) + " / " + month + " / " + year;
            } else if (date == 1) {
                document.getElementById("output").innerHTML = (date + 30) + " / " + (month + 11) + " / " + (year - 1);
            }
            break;
        case 3: case 5: case 7: case 8: case 10: case 12:
            if (1 < date && date <= 31) {
                document.getElementById("output").innerHTML = (date - 1) + " / " + month + " / " + year;
            } else if (date == 1) {
                document.getElementById("output").innerHTML = "30 / " + (month - 1) + " / " + year;
            }
            break;
        case 2:
            //Check / Nhuan ==> Co 29 ngay (2012, 2016, 2020)
            if (year % 4 == 0 && year / 100 != 0 || year % 400 == 0) {
                if (1 < date && date <= 29) {
                    document.getElementById("output").innerHTML = (date - 1) + " / " + month + " / " + year;
                } else if (date == 1) {
                    document.getElementById("output").innerHTML = "31" + " / " + (month - 1) + " / " + year;
                }
            }
            else {
                if (1 < date && date <= 28) {
                    document.getElementById("output").innerHTML = (date - 1) + " / " + month + " / " + year;
                } else if (date == 1) {
                    document.getElementById("output").innerHTML = "31 / " + (month - 1) + " / " + year;
                }
            }
            break;
        case 4: case 6: case 9: case 11:
            if (1 < date && date <= 29) {
                document.getElementById("output").innerHTML = (date - 1) + " / " + month + " / " + year;
            } else if (date == 1) {
                document.getElementById("output").innerHTML = " 30 / " + (month - 1) + " / " + year;
            }
            break;
        default:
            alert("Nhap sai so");
    }
}

// Bai Tap 6
function tinhNgay() {
    var m = document.getElementById('txt-m').value * 1;
    var y = document.getElementById('txt-y').value * 1;

    switch (m) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            document.getElementById("output-display").innerHTML = `Thang ${m} nam ${y} co 31 ngay`;

            break;
        case 2:
            if (y % 4 == 0 && y / 100 != 0 || y % 400 == 0) {
                document.getElementById("output-display").innerHTML = `Thang ${m} nam ${y} co 29 ngay`;
            } else {
                document.getElementById("output-display").innerHTML = `Thang ${m} nam ${y} co 28 ngay`;
            }
            break;
        case 4: case 6: case 9: case 11:
            document.getElementById("output-display").innerHTML = `Thang ${m} nam ${y} co 30 ngay`;
            break;
        default:
            alert("Nhap sai so roi");
    }
}

// Bai Tap 7
function docSo() {
    var number = document.getElementById('txt-so-bt-7').value * 1;
    var tram = Math.floor(number / 100);
    var chuc = Math.floor(number % 100 / 10);
    var dv = number % 10;
    var tr, ch, donv, l;
    if (number < 100 || number > 999) {
        alert("Nhap so co 3 chu so");
    }
    else {
        switch (tram) {
            case 1: tr = innerHTML = `Một Trăm`; break;
            case 2: tr = innerHTML = `Hai Trăm`; break;
            case 3: tr = innerHTML = `Ba Trăm`; break;
            case 4: tr = innerHTML = `Bốn Trăm`; break;
            case 5: tr = innerHTML = `Năm Trăm`; break;
            case 6: tr = innerHTML = `Sáu Trăm`; break;
            case 7: tr = innerHTML = `Bảy Trăm`; break;
            case 8: tr = innerHTML = `Tám Trăm`; break;
            case 9: tr = innerHTML = `Chín Trăm`; break;
        }
        if (chuc % 10 == 0 && dv != 0) {
            ch = innerHTML = `Le `;
        } else {
            switch (chuc) {
                case 0: ch = innerHTML = ` `; break;
                case 1: ch = innerHTML = `Mười `; break;
                case 2: ch = innerHTML = `Hai Mươi`; break;
                case 3: ch = innerHTML = `Ba Mươi`; break;
                case 4: ch = innerHTML = `Bốn Mươi`; break;
                case 5: ch = innerHTML = `Năm Mươi`; break;
                case 6: ch = innerHTML = `Sáu Mươi`; break;
                case 7: ch = innerHTML = `Bảy Mươi`; break;
                case 8: ch = innerHTML = `Tám Mươi`; break;
                case 9: ch = innerHTML = `Chín Mươi`; break;
            }
        }
        switch (dv) {
            case 0: donv = innerHTML = ` `; break;
            case 1: donv = innerHTML = `Một `; break;
            case 2: donv = innerHTML = `Hai`; break;
            case 3: donv = innerHTML = `Ba`; break;
            case 4: donv = innerHTML = `Bốn`; break;
            case 5: donv = innerHTML = `Năm`; break;
            case 6: donv = innerHTML = `Sáu`; break;
            case 7: donv = innerHTML = `Bảy`; break;
            case 8: donv = innerHTML = `Tám`; break;
            case 9: donv = innerHTML = `Chín`; break;
        }
    }
    document.getElementById('testOP').innerHTML = `${tr}  ${ch}  ${donv}`;
}

// Bai Tap 8




function tinhToaDo(x1, y1, x2, y2) {
    var temp, dist;
    temp = Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2);
    return dist = Math.sqrt(temp);
}
function kiemTraKhoangCach() {

    // Hoc sinh 1
    var name1 = document.getElementById('txt-name-hs1').value;
    var cx1 = document.getElementById('txt-cord1-x').value * 1;
    var cy1 = document.getElementById('txt-cord1-y').value * 1;

    // Hoc sinh 2
    var name2 = document.getElementById('txt-name-hs2').value;
    var cx2 = document.getElementById('txt-cord2-x').value * 1;
    var cy2 = document.getElementById('txt-cord2-y').value * 1;

    // Hoc sinh 3
    var name3 = document.getElementById('txt-name-hs3').value;
    var cx3 = document.getElementById('txt-cord3-x').value * 1;
    var cy3 = document.getElementById('txt-cord3-y').value * 1;

    // Truong hoc
    var i = document.getElementById('txt-school-cord-x').value * 1;
    var z = document.getElementById('txt-school-cord-y').value * 1;

    var hs1 = tinhToaDo(cx1, cy1, i, z);
    var hs2 = tinhToaDo(cx2, cy2, i, z);
    var hs3 = tinhToaDo(cx3, cy3, i, z);

    if (hs1 > hs2 && hs1 > hs3) {
        document.getElementById("testBT8").innerHTML = `HS 1 - ${name1} xa truong nhat`;
    } else if (hs2 > hs1 && hs2 > hs3) {
        document.getElementById("testBT8").innerHTML = `HS 2 - ${name2} xa truong nhat`;
    } else if (hs3 > hs1 && hs3 > hs2) {
        document.getElementById("testBT8").innerHTML = `HS 3 - ${name3} xa truong nhat`;
    }

}


